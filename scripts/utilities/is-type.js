// scripts/utilities/is-type.js

/** ### Determines whether a value is any kind of JavaScript object or instance
 *
 * Compare with isPlainObjOrInst().
 *
 * @example
 *
 * isAnyObjOrInst(null); // false
 * isAnyObjOrInst([]); // false
 *
 * isAnyObjOrInst(/rx/); // true
 * isAnyObjOrInst(new RegExp('rx')); // true
 * isAnyObjOrInst({}); // true
 * class Animal { constructor(legs) { this.legs = legs } };
 * isAnyObjOrInst(new Animal(4)); // true
 *
 * @param {object} value
 * @returns {boolean}
 */
export const isAnyObjOrInst = (value) =>
    value != null && typeof value === 'object' && ! Array.isArray(value);

/** ### Determines whether a value is a plain JavaScript object or instance
 *
 * Compare with isAnyObjOrInst().
 *
 * @example
 *
 * isPlainObjOrInst(null); // false
 * isPlainObjOrInst([]); // false
 * isPlainObjOrInst(/rx/); // false
 * isPlainObjOrInst(new RegExp('rx')); // false
 *
 * isPlainObjOrInst({}); // true
 * class Animal { constructor(legs) { this.legs = legs } };
 * isPlainObjOrInst(new Animal(4)); // true
 *
 * @param {object} value
 * @returns {boolean}
 */
export const isPlainObjOrInst = (value) =>
    Object.prototype.toString.call(value) === '[object Object]';


/* ---------------------------------- TEST ---------------------------------- */

export const testIsType = (equal) => {

    // Prepare some classes and instances.
    class Foo {};
    const inst = new Foo();
    class Animal {
        constructor(legs) { this.legs = legs; this.canFly = false }
        toString() { return `Has ${this.legs} legs, can${this.canFly ? '' : 'not'} fly` }
    };
    class Bird extends Animal {
        constructor(canFly) { super(2); this.canFly = canFly }
    };
    const cat = new Animal(4);
    const canary = new Bird(true);
    equal(String(cat), 'Has 4 legs, cannot fly');
    equal(canary.toString(), 'Has 2 legs, can fly');

    // Test that isAnyObjOrInst() returns `false` as expected.
    equal(isAnyObjOrInst(Foo), false);
    equal(isAnyObjOrInst(), false);
    equal(isAnyObjOrInst(0), false);
    equal(isAnyObjOrInst(Number(0)), false);
    equal(isAnyObjOrInst(null), false);
    equal(isAnyObjOrInst([]), false);
    equal(isAnyObjOrInst(Date), false);

    // Test that isAnyObjOrInst() returns `true` as expected.
    equal(isAnyObjOrInst(new Object(0)), true);
    equal(isAnyObjOrInst(/abc/), true);
    equal(isAnyObjOrInst(Math), true);
    equal(isAnyObjOrInst(new Date()), true);
    equal(isAnyObjOrInst(new RegExp('abc')), true);
    equal(isAnyObjOrInst(Error('oops')), true);
    equal(isAnyObjOrInst(inst), true);
    equal(isAnyObjOrInst({}), true);
    equal(isAnyObjOrInst(Object({})), true);
    equal(isAnyObjOrInst(JSON.parse('{"a":1}')), true);

    // Test that isAnyObjOrInst() handles more complex classes as expected.
    equal(isAnyObjOrInst(cat), true);
    equal(isAnyObjOrInst(canary), true);

    // Test that isPlainObjOrInst() returns `false` as expected.
    equal(isPlainObjOrInst(Foo), false);
    equal(isPlainObjOrInst(), false);
    equal(isPlainObjOrInst(0), false);
    equal(isPlainObjOrInst(Number(0)), false);
    equal(isPlainObjOrInst(null), false);
    equal(isPlainObjOrInst([]), false);
    equal(isPlainObjOrInst(Date), false);
    equal(isPlainObjOrInst(new Object(0)), false);
    equal(isPlainObjOrInst(/abc/), false);
    equal(isPlainObjOrInst(Math), false);
    equal(isPlainObjOrInst(new Date()), false);
    equal(isPlainObjOrInst(new RegExp('abc')), false);
    equal(isPlainObjOrInst(Error('oops')), false);

    // Test that isPlainObjOrInst() returns `true` as expected.
    equal(isPlainObjOrInst(inst), true);
    equal(isPlainObjOrInst({}), true);
    equal(isPlainObjOrInst(Object({})), true);
    equal(isPlainObjOrInst(JSON.parse('{"a":1}')), true);

    // Test that isPlainObjOrInst() handles more complex classes as expected.
    equal(isPlainObjOrInst(cat), true);
    equal(isPlainObjOrInst(canary), true);
}
