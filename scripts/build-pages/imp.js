// scripts/build-pages/imp.js

import { isPlainObjOrInst } from '../utilities/is-type.js';

/** ### Inlines dynamic JavaScript `await import()` statements, in an HTML file
 *
 * @param   {object}  cache    Dictionary of CSS and JS previously read from disk
 * @param   {object}  deps     Node.js's dirname(), readFileSync(), resolve()
 * @param   {string}  path     Path to the HTML file
 * @param   {string}  rawHtml  The HTML file's entire unprocessed contents
 * @throws  {Error|RangeError|TypeError}
 * @return  {string}           HTML with dynamic `import()` resolved and inlined
 */
export const imp = (cache, deps, path, rawHtml) => {
    const pf = 'imp(): ';

    // Validate the arguments.
    validateCache(pf, cache);
    validateDeps(pf, deps);
    validatePathAndRawHtml(pf, path, rawHtml);

    // Get the string which all relative import paths should start with.
    // This is for the majority of import statements, eg `import('js/app.js')`.
    // The special `await import('/dist/.../my_app.js')` is treated differently.
    const { dirname, resolve } = deps;
    let srcPP = dirname(resolve(path)).replace(/\\/g, '/'); // source path prefix
    while (srcPP.slice(-10) !== '/templates') {
        srcPP = srcPP.split('/').slice(0, -1).join('/');
        if (srcPP === '') throw RangeError(`${pf}path has no '/templates'`);
    }
    const scrPPLen = srcPP.length;

    // Find all the `<script>` elements in the HTML - ignore `<script src="...">
    // or `<script type="module">`, etc.
    return rawHtml
        .split('<script>')
        .map((scriptPart, i) => {
            if (i === 0) return scriptPart; // before the first '<script>'

            // Get the inline JavaScript.
            const jsEndPos = scriptPart.indexOf('</script>');
            if (jsEndPos === -1) throw RangeError(
                `${pf}Cannot find end of '<script>' [${i}] in:\n    ${path}`);
            const rawJs = scriptPart.slice(0, jsEndPos);
            const afterJs = scriptPart.slice(jsEndPos);

            // Ignore the JavaScript if it doesn't look processable. It should:
            // - Begin with "! async function( ..."
            // - Contain at least one "await import('" or 'await import("'
            // - Not contain "export ...'
            // TODO maybe not contain `return` on the top level?
            if (
                ! /^\s*!\s*async\s+function\s*\(/.test(rawJs)
                || ! /\bawait\s+import\s*\(\s*['"]/.test(rawJs)
                || /\bexport\s+/.test(rawJs)
            ) return rawJs + afterJs;

            // Remove lines beginning "/**/", and code between "/*-*/ ... /*+*/".
            const productionJs = removeDevOnlyCode(rawJs);

            // Change 'await import("...")' to 'imp("...")', and similarly
            // change "await import('...')" to "imp('...')". At the same time,
            // store all JavaScript import paths in `importPaths`.
            // Note that `rx` ends with the global flag `g`, so is 'sticky'.
            const rx = /(\bawait\s+import\s*\(\s*['"])([-.\\/\w]+\.js)(['"]\s*\))/g;
            let processedJs = [], prevEndPos = 0, match;
            const importPaths = [];
            while ((match = rx.exec(productionJs)) !== null) {
                const [ _, jsBefore, importPath, jsAfter ] = match;

                // Resolve the import path. A distribution JavaScript file like
                // `import('/dist/.../foo.js')` resolves to 'dist/.../foo.js'.
                // A source file like `import('js/app.js')` resolves to
                // 'src/pages/templates/js/app.js'.
                let riPath; // resolved import path
                if (importPath.slice(0, 6) === '/dist/') {
                    riPath = importPath.slice(1); // trim leading "/"
                } else {
                    const resolvedImportPath =
                        deps.resolve(deps.dirname(path), importPath)
                            .replace(/\\/g, '/');
                    if (resolvedImportPath.slice(0, scrPPLen) !== srcPP)
                        throw Error(`${pf}"${resolvedImportPath}" does not begin "${srcPP}"`);
                    riPath = resolvedImportPath.slice(scrPPLen);
                }

                // Add this path to the list of imports, if it hasn't been seen
                // before. These will be inlined by generateImpFunction(), below.
                if (! importPaths.includes(riPath)) importPaths.push(riPath);

                // Write code like `const { App } = imp('/js/app.js')`.
                processedJs.push(productionJs.slice(prevEndPos, match.index));
                const jsBeforeImp = jsBefore.replace(/^await\s+import/, 'imp');
                processedJs.push(`${jsBeforeImp}${riPath}${jsAfter}`);
                prevEndPos = rx.lastIndex;
            }
            processedJs.push(productionJs.slice(prevEndPos));

            // Detect the indent to use for the generated `imp()` function.
            const lines = processedJs.join('').split('\n');
            const indent = lines
                .slice(1, -1) // the first and last lines don't count
                .reduce((minInd, line) => {
                    const [ ind ] = line.match(/^\s*/);
                    return ind.length < minInd.length ? ind : minInd;
                }, ' '.repeat(16));

            // Recursively walk the `import()` tree, to build up a complete list
            // of imported code.
            importPaths.forEach(importPath => readFromFileOrCache(
                pf, cache, srcPP, deps, importPath, importPaths));

            // Generate and insert the `imp()` function at the top of the script
            // just after the first line.
            return lines[0] // keep the first line as-is, eg `!async function (){`
                + generateImpFunction(pf, cache, importPaths, indent)
                + inlineWasm(deps, lines.slice(1).join('\n'))
                + afterJs;
        })
        .join('<script>');
}


/* --------------------------------- PRIVATE -------------------------------- */

// Validates the `cache` argument passed to imp().
const validateCache = (pf, cache) => {
    if (! isPlainObjOrInst(cache)) throw TypeError(
        `${pf}cache is not a plain object or instance`);
    Object.entries(cache).forEach(([ key, value ]) => {
        const ext = key.split('.').pop();
        if (ext !== 'css' && ext !== 'js') throw TypeError(
            `${pf}cache['${key}'] has extension '${ext}' not 'css' or 'js'`);
        if (ext === 'css') {
            if (typeof value !== 'string') throw TypeError(
               `${pf}cache['${key}'] is type '${typeof value}' not 'string'`);
            return;
        }
        if (! isPlainObjOrInst(value)) throw TypeError(
            `${pf}cache['${key}'] is type '${typeof value}' not 'object'`);
        if (typeof value.code !== 'string') throw TypeError(
            `${pf}cache['${key}'].code is type '${typeof value}' not 'string'`);
        if (! Array.isArray(value.subImportPaths)) throw TypeError(
            `${pf}cache['${key}'].subImportPaths is type '${typeof value.subImportPaths}' not an array`);
        // TODO validate each subImportPath
    });
}

// Validates the dependencies `deps`, injected into imp().
const validateDeps = (pf, deps) => {
    if (! isPlainObjOrInst(deps)) throw TypeError(
        `${pf}deps is not a plain object or instance`);
    const { dirname, readFileSync, resolve } = deps;
    if (typeof dirname !== 'function') throw TypeError(
        `${pf}deps.dirname is type '${typeof dirname}' not 'function'`);
    if (typeof readFileSync !== 'function') throw TypeError(
        `${pf}deps.readFileSync is type '${typeof readFileSync}' not 'function'`);
    if (typeof resolve !== 'function') throw TypeError(
        `${pf}deps.resolve is type '${typeof resolve}' not 'function'`);
}

// Validates the `path` and `rawHtml` arguments passed to imp().
const validatePathAndRawHtml = (pf, path, rawHtml) => {
    if (typeof path !== 'string') throw TypeError(
        `${pf}path is type '${typeof path}' not 'string'`);
    if (path.slice(-5) !== '.html') throw RangeError(
        `${pf}path ends '${path.slice(-5)}' not '.html'`);
    if (typeof rawHtml !== 'string') throw TypeError(
        `${pf}rawHtml is type '${typeof rawHtml}' not 'string'`);
    if (rawHtml.length === 0) throw RangeError(
        `${pf}rawHtml at '${path}' is empty`);
    if (! rawHtml.includes('<!DOCTYPE html>')) throw RangeError(
        `${pf}rawHtml at '${path}' does not contain '<!DOCTYPE html>'`);
}

// Wraps a string in quotes, ready to be inserted into JavaScript code.
const quote = (text) => {
    if (! text.includes("'")) return `'${text}'`; // prefer single quotes
    if (! text.includes('"')) return `"${text}"`;
    return `'${text.replace(/'/g, "\\'")}'`;
}

// Generates the `imp()` function, based on a list of `import()` paths.
const generateImpFunction = (pf, cache, importPaths, indent) => {

    // Read each `import()` path's code from the cache, and place each block of
    // code inside a function.
    const files = importPaths.flatMap(importPath => {
        if (! cache[importPath]) throw RangeError( // TODO remove if unreachable
            `${pf}Missing cache["${importPath}"]`);
        return [
            indent + '        ' + quote(importPath) + ': () => {',
            cache[importPath].code,
            indent + '        },',
        ];
    });

    // Generate and return the `imp()` function.
    return '\n' + [
        indent + 'const imp = path => {',
        indent + '    const files = {',
        ...files,
        indent + '    };',
        indent + "    if (! files[path]) throw RangeError('imp(): 48701 ' + path);",
        indent + '    return files[path]();',
        indent + '};',
    ].join('\n') + '\n\n';
}

// Replaces each `await wasm.default('/dist/.../my_app.wasm');` with:
//     wasm.initAsync(`data:application/wasm;base64, ...`);
// TODO cache `readFileSync(path)`, it's fetched again for every language
// TODO use `slice()` with indexes, not `new RegExp()`.
const inlineWasm = (deps, topLevelJs) => {
    const { readFileSync } = deps;
    let processedJs = topLevelJs;

    // Note that `rx` ends with the global flag `g`, so is 'sticky'.
    const rx = /\bawait\s+wasm\.default\s*\(\s*['"]\/(dist\/[^'"]+\.wasm)['"]\s*\)\s*;/g;
    let match;
    while ((match = rx.exec(topLevelJs)) !== null) {
        const [ _, path ] = match;
        const wasm = readFileSync(path); // TODO handle error
        const encoded = Buffer.from(wasm, 'binary').toString('base64');
        const chunks = encoded.split('').reduce((acc, char, i) => i % 1000
            ? (acc[acc.length-1] = acc.at(-1)+char) && acc : [...acc, [char]], []);
        const dataUrlWasm = `data:application/wasm;base64,\n${chunks.join('\n')}`;
        const escPath = path.replace(/\./g, '\\.'); // escape "."
        processedJs = processedJs.replace(
            new RegExp(`\\bawait\\s+wasm\\.default\\s*\\(\\s*['"]\\/${escPath}['"]\\s*\\)\\s*;`),
            'await wasm.initAsync(`' + dataUrlWasm + '`);',
        );
    }
    return processedJs;
}

// Returns processed JavaScript code if previously added to the cache.
// Otherwise, reads the code from disk, processes it, caches and returns it.
// readFromFileOrCache() also adds any missing sub-dependencies to `importPaths`.
// The processing step makes this function recursive, through the chain:
// - inlineJsModule() which calls...
// - changeDynamicImportToImp() which calls...
// - readFromFileOrCache()
const readFromFileOrCache = (pf, cache, srcPP, deps, importPath, importPaths) => {
    const { dirname, readFileSync } = deps;

    // If the code has already been processed and cached, add any missing
    // sub-dependencies to `importPaths` and return then processed code.
    const resolvedPath = importPath[0] === '/' ? srcPP + importPath : importPath;
    if (cache[importPath]) {
        const { code, subImportPaths } = cache[importPath];
        subImportPaths.forEach(
            subImportPath => {
                if (! importPaths.includes(subImportPath))
                    importPaths.push(subImportPath);
            }
        );
        return code;
    }

    // Read the raw code from disk.
    let rawJs; try {
        rawJs = String(readFileSync(resolvedPath));
    } catch(err) {
        throw Error(`${pf}Cannot read '${importPath}':\n    ${err.message}`) }

    // Reject the JavaScript module if it has a `return` in the top level.
    // TODO

    // Prepare wasm bindgen JavaScript, if this is 'dist/.../my_rust_app.js'.
    // Else, remove lines beginning "/**/", and code between "/*-*/ ... /*+*/".
    const productionJs =
        importPath.slice(0, 5) === 'dist/' && rawJs.slice(0, 9) === 'let wasm;'
            ? prepareWasmBindgen(rawJs)
            : removeDevOnlyCode(rawJs);

    // Remove each "export " string, and append eg "return { a, b, c };".
    const jsNoExports = removeExports(pf, productionJs);

    // Change dynamic imports like "await import('./a.js')" to "imp('./a.js')".
    // This is part of a recursive chain of three functions.
    const codeAndSubImports = changeDynamicImportToImp
        (pf, cache, srcPP, deps, importPaths, jsNoExports, dirname(resolvedPath));

    // Cache the processed code. Make sure that the current `import()` path is
    // included in the list of paths, for generateImpFunction() to use.
    cache[importPath] = codeAndSubImports;
    if (! importPaths.includes(importPath)) importPaths.push(importPath);
    return codeAndSubImports.code;
}

// Prepares wasm bindgen JavaScript, from a file like 'dist/.../my_rust_app.js'.
const prepareWasmBindgen = originalJs => {
    return originalJs
        .replace('\nexport { initSync }\n', '\n// export { initSync }\n')
        .replace('\nexport default __wbg_init;\n', '\n// export default __wbg_init;\n')
        .replace(
            /input = new URL\('[_a-z0-9]+\.wasm', import\.meta\.url\);/,
            '// Line removed by prepareWasmBindgen() - the `import.meta` would fail,\n' +
            '        // because this is no longer an imported context.'
        )
        // TODO remove `function initSync(module) { ... }` and maybe others
        + '\n\nexport const initAsync = __wbg_init;\n'
    ;
}

// Removes lines beginning "/**/", and code between "/*-*/ ... /*+*/".
const removeDevOnlyCode = originalJs => {
    // The FORMFEED character will be used as a marker, so throw an exception if
    // `originalJs` already contains FORMFEED characters.
    if (originalJs.includes('\f')) throw RangeError(
        'removeDevOnlyCode(): originalJs contains a FORMFEED character')

    // Bail out early if there are no block comments in the code.
    const parts = originalJs.split('/*');
    if (parts.length === 1) return originalJs;

    const partsWithMarkers = parts
        .map((part, i) => {
            if (i === 0) return part; // before the first '/*'

            // Bail out early if the part cannot be "/**/", "/*-*/" or "/*+*/".
            const secondIsAsterisk = part[1] === '*';
            const secondIsSlash = ! secondIsAsterisk && part[1] === '/';
            if (! secondIsAsterisk && ! secondIsSlash) return '/*' + part;

            // Deal with either "/*X/" or "/**/".
            if (secondIsSlash) {
                if (part[0] !== '*') return '/*' + part; // "/*X/"
                const nlPos = part.indexOf('\n');
                return nlPos < 0 // remove everything...
                    ? '\f' // ...up to the end of the part...
                    : '\f' + part.slice(nlPos); // ...or just up to the newline
            }

            // Here, `secondIsAsterisk` must be `true`, so `part[1]` must be "*".
            // Deal with "/*-*X", "/****", etc.
            if (part[2] !== '/') return '/*' + part;

            // "/*-*/" starts a run to remove.
            if (part[0] === '-') return '\f';

            // "/*+*/" ends a run to remove.
            if (part[0] === '+') return '\f' + part.slice(3);

            // This just catches an edge case like "/*X*/".
            return '/*' + part;
        })
        .join('');

    // Remove lines which contain only whitespace and also contain a FORMFEED.
    // Then, delete any remaining FORMFEED characters.
    return partsWithMarkers
        .split('\n')
        .filter(line => ! line.includes('\f') || ! /^\s+$/.test(line))
        .join('\n')
        .replace(/\f/g, '');
}

// Changes exported declarations like "export const a = 1" to "const a = 1".
// https://developer.mozilla.org/en-US/docs/web/javascript/reference/statements/export#syntax
// Also, inserts exported identifiers into a returned object, eg "return { a }".
// TODO allow "const a = 1; export a"
const removeExports = (pf, originalJs) => {
    let prevEndPos = 0, match;
    const processedJs = [], exportedIdentifiers = [];

    // Note that `rx` ends with the global flag `g`, so is 'sticky'.
    const rx = /\bexport\s+((class|const|function\*?|let|var)\s+([$_A-Za-z][$_0-9A-Za-z]*))\b/g;

    // Remove each instance of "export", and record each exported identifier.
    while ((match = rx.exec(originalJs)) !== null) {
        const [ _, keywordAndIdentifier, _keyword, identifier ] = match;
        if (exportedIdentifiers.includes(identifier)) throw RangeError(
            `${pf}identifier '${identifier}' appears twice in ${resolvedPath}`);
            exportedIdentifiers.push(identifier);
        processedJs.push(originalJs.slice(prevEndPos, match.index));
        processedJs.push(keywordAndIdentifier);
        prevEndPos = rx.lastIndex;
    }
    processedJs.push(originalJs.slice(prevEndPos));

    // Insert exported identifiers into a returned object, eg "return { a }".
    processedJs.push(`\nreturn { ${exportedIdentifiers.join(', ')} };`);
    return processedJs.join('');
}

// Changes dynamic imports like "await import('./a.js')" to "imp('./a.js')".
const changeDynamicImportToImp = (pf, cache, srcPP, deps, importPaths, jsNoExports, pathDirs) => {
    const scrPPLen = srcPP.length;
    const newlyFoundImportPaths = [];
    let processedJs = [], prevEndPos = 0, match;

    // Note that `rx` ends with the global flag `g`, so is 'sticky'.
    const rx = /(\bawait\s+import\s*\(\s*['"])([-.\\/\w]+\.js)(['"]\s*\))/g;

    // Change each instance of "await import" to "imp", and record each path.
    while ((match = rx.exec(jsNoExports)) !== null) {
        const [ _, jsBefore, importPath, jsAfter ] = match;
        const resolvedImportPath = deps.resolve(pathDirs, importPath).replace(/\\/g, '/');
        if (resolvedImportPath.slice(0, scrPPLen) !== srcPP)
            throw Error(`${pf}"${resolvedImportPath}" does not begin "${srcPP}"`);
        const riPath = resolvedImportPath.slice(scrPPLen); // resolved import path
        if (!importPaths.includes(riPath)) newlyFoundImportPaths.push(riPath);
        processedJs.push(jsNoExports.slice(prevEndPos, match.index));
        const jsBeforeImp = jsBefore.replace(/^await\s+import/, 'imp');
        processedJs.push(`${jsBeforeImp}${riPath}${jsAfter}`);
        prevEndPos = rx.lastIndex;
    }
    processedJs.push(jsNoExports.slice(prevEndPos));

    // Recursively add each imported file to the cache.
    newlyFoundImportPaths.forEach(resolvedImportPath => {
        readFromFileOrCache(pf, cache, srcPP, deps, resolvedImportPath, importPaths)});

    return {
        code: processedJs.join(''),
        subImportPaths: newlyFoundImportPaths,
    };
}
