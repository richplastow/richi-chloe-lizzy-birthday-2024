// scripts/build-pages/cache-templates.js

import { imp } from './imp.js';

/** ### Reads all of the templates from file into memory
 *
 * Also checks that the templates exist, and are valid.
 *
 * And also inlines CSS and JavaScript.
 *
 * @param   {string}  absPathTemplates  Path to the 'templates/' folder
 * @param   {object}  deps  Injected standard Node functions
 * @throws  {Error}
 * @returns {object}
 */
export const cacheTemplates = (absPathTemplates, { dirname, readdirSync, readFileSync, resolve }) => {
    const pf = 'cacheTemplates(): ';

    // Get the filenames of all HTML files in the src/pages/templates/ folder.
    const templateItems = readdirSync(absPathTemplates)
        .filter(item => item.slice(-5) === '.html');

    const cssAndJsCache = {};

    // Try to read each HTML template, and roughly validate its syntax.
    return templateItems.reduce((cache, templateName) => ({
        ...cache,
        [templateName]: readValidateAndInlineHTML(
            pf,
            absPathTemplates,
            dirname,
            readFileSync,
            resolve,
            templateName,
            cssAndJsCache,
        ),
    }), {});
};


/* --------------------------------- PRIVATE -------------------------------- */

const readValidateAndInlineHTML = (pf, absPathTemplates, dirname, readFileSync, resolve, templateName, cssAndJsCache) => {
    const path = resolve(absPathTemplates, templateName);

    // Read the template HTML file.
    let html;
    try {
        html = String(readFileSync(path));
    } catch (err) {
        throw Error(`${pf}Cannot read '${path}':\n    ${err.message}`) }

    // Check that it looks basically like an HTML file.
    const trimmed = html.trim();
    if (trimmed.length === 0) throw Error(
        `${pf}'${path}' is empty`);
    if (! trimmed.includes('<!DOCTYPE html>')) throw Error(
        `${pf}'${path}' does not contain '<!DOCTYPE html>'`);

    // Inline any CSS and JavaScript imports.
    const alreadyImported = [];
    const withInlinedCss = inlineImportedCss(
        pf, dirname, readFileSync, resolve, path, trimmed, alreadyImported, cssAndJsCache);
    const withInlinedJs = imp(cssAndJsCache, { dirname, readFileSync, resolve }, path, withInlinedCss);

    return withInlinedJs;
};

const inlineImportedCss = (pf, dirname, readFileSync, resolve, path, htmlOrCss, alreadyImported, cssAndJsCache) => {
    return htmlOrCss
        .split("@import url('")
        .map((part, i) => {
            if (i === 0) return part; // before the first "@import url('"

            // Get the path to the imported CSS.
            const cssPathEndPos = part.indexOf("');");
            if (cssPathEndPos === -1) throw RangeError(
                `${pf}Cannot find end of "@import url('" [${i}] in:\n    ${path}`);
            const cssPath = part.slice(0, cssPathEndPos);
            if (cssPath.slice(-4) !== '.css') throw RangeError(
                `${pf}Invalid cssPath '${cssPath}' [${i}] does not end '.css' in:\n    ${outputPath}`);
            const resolvedPath = resolve(dirname(path), cssPath);

            // Prevent infinite recursion.
            if (alreadyImported.includes(resolvedPath))
                return '/* [removed recursive import] */\n' + part.slice(cssPathEndPos + 3);
            alreadyImported.push(resolvedPath);

            // Read the imported CSS file, or retrieve it from the cache.
            if (! cssAndJsCache[resolvedPath]) {
                try {
                    cssAndJsCache[resolvedPath] = String(readFileSync(resolvedPath));
                } catch (err) {
                    throw Error(`${pf}Cannot read '${resolvedPath}':\n    ${err.message}`) }
            }
            const css = cssAndJsCache[resolvedPath];

            // Recursively inline any `@import` statements inside this CSS file.
            const recursedCss = inlineImportedCss(pf, dirname, readFileSync, resolve,
                resolvedPath, css, alreadyImported, cssAndJsCache);

            return recursedCss + part.slice(cssPathEndPos + 3);
        })
        .join('');
}
