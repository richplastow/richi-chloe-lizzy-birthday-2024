// scripts/build-pages/build-page.js

import { renderSidebarPage } from './render-sidebar-page.js';

/** ### Generates an HTML file
 *
 * @param   {object}  contentItem
 * @param   {object}  deps  Injected standard Node functions
 * @param   {object}  templates
 * @param   {string}  outputDir
 */
export const buildPage = (contentItem, { dirname, mkdirSync, resolve, writeFileSync }, templates, outputDir) => {
    const pf = 'buildPage(): ';
    const template = templates[contentItem.template] || templates['default.html'];
    const { route } = contentItem.frontmatter;

    //
    const renderedPage = contentItem.hasSidebar
        ? renderSidebarPage(pf, contentItem.frontmatter, contentItem.html, template)
        : renderContentPage(contentItem.frontmatter, contentItem.html, 'en', route, template);

    const outputPath = resolve(outputDir, route);

    try {
        mkdirSync(dirname(outputPath), { recursive: true });
        writeFileSync(outputPath, renderedPage, {});
    } catch (err) {
        throw Error(`${pf}Cannot write '${outputPath}':\n    ${err.message}`) }
};


/* --------------------------------- PRIVATE -------------------------------- */

const renderContentPage = (frontmatter, html, languageCode, route, template) => {
    return template
        .replace(/{{routeBasename}}/g, route.split('.')[0])
        .replace(/{{languageCode}}/g, languageCode)
        .replace(/{{title}}/g, `${frontmatter.title}`)
        .replace('{{content}}', html)
    ;
}
