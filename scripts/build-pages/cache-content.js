// scripts/build-pages/cache-content.js

import { ruffMarkdownToHtml } from './ruff-markdown-to-html.js';

/** ### Reads all of the Markdown content from file into memory
 *
 * Also checks that the .md files exist, and are valid.
 *
 * @param   {string}  absPathContent  Absolute path to the content folder
 * @param   {object}  deps  Injected standard Node functions
 * @throws  {Error}
 * @returns {object}
 */
export const cacheContent = (absPathContent, { readdirSync, readFileSync, resolve }) => {
    const pf = 'cacheContent(): ';

    // Get the filenames of all markdown files in the src/pages/content/ folder.
    const contentFilenames = readdirSync(absPathContent)
        .filter(item => item.slice(-3) === '.md');

    // Try to read each Markdown file, and roughly validate its syntax.
    return contentFilenames.reduce((cache, contentFilename) => ({
        ...cache,
        [contentFilename]: readValidateAndParseMarkdown(
            pf,
            absPathContent,
            contentFilename,
            readFileSync,
            resolve,
        ),
    }), {});
};


/* --------------------------------- PRIVATE -------------------------------- */

const defaultFrontmatter = {
    title: 'Untitled',
};

const readValidateAndParseMarkdown = (pf, absPathContent, contentFilename, readFileSync, resolve) => {
    const path = resolve(absPathContent, contentFilename);

    // Try to read the Markdown file.
    let md; try {
        md = String(readFileSync(path));
    } catch (err) {
        throw Error(`${pf}Cannot read '${path}':\n    ${err.message}`);
    }
    const trimmed = md.trim() + '\n';
    if (trimmed.length === 1) throw Error(
        `${pf}'${path}' is empty`);

    // Do some basic validation of the frontmatter.
    if (trimmed.slice(0, 4) !== '---\n') throw Error(
        `${pf}'${path}' does not start "---" followed by a newline`);
    const frontmatterEndPos = trimmed.indexOf('\n---\n', 3);
    if (frontmatterEndPos === -1) throw Error(
        `${pf}'${path}' has no "---" surrounded by newlines to end the frontmatter`);
    if (frontmatterEndPos === 3) throw Error(
        `${pf}'${path}' has no frontmatter`);
    const frontmatterRaw = trimmed.slice(4, frontmatterEndPos).trim();
    if (frontmatterRaw.length === 0) throw Error(
        `${pf}'${path}' has empty frontmatter`);

    // Parse and further validate the frontmatter.
    const frontmatter = {
        ...defaultFrontmatter,
        ...Object.fromEntries(
            frontmatterRaw
                .split('\n')
                .map(line => line.trim())
                .filter(line => line.length !== 0)
                .map(line => (line.match(/^([a-z]+):\s*(.+)$/) || []).slice(1, 3))
                .filter(entry => entry.length === 2)
        )
    };
    validateFrontmatterObject(`${pf}${contentFilename} `, frontmatter);

    // Transform the Markdown to HTML.
    const html =
        `<!-- ${contentFilename} -->\n` +
        ruffMarkdownToHtml(
            trimmed.slice(frontmatterEndPos + 5).trim() + '\n\n'
        );

    return { frontmatter, html};
};

const validateFrontmatterObject = (pf, frontmatter) => {
    if (frontmatter.title.length < 3) throw RangeError(
        `${pf}frontmatter title is less than 3 characters`);
    // TODO more validations
}
