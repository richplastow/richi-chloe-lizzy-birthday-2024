// scripts/build-pages/ruff-markdown-to-html.js

/** ### Roughly transforms Markdown to HTML
 *
 * A cheap-n-cheerful Markdown renderer, which relies on the markdown conforming
 * to a particular happy path. For example, stick to ASCII characters.
 *
 * @param {string} markdown
 * @returns {string}
 */
export const ruffMarkdownToHtml = markdown =>
    ('\n'+markdown+'\n\n') // wrap in newline, to simplify logic
        .replace(/\n###### ([^\n]+)/g, (_, text) =>
            `\n<h6>${text}</h6>`)
        .replace(/\n##### ([^\n]+)/g, (_, text) =>
            `\n<h5>${text}</h5>`)
        .replace(/\n#### ([^\n]+)/g, (_, text) =>
            `\n<h4>${text}</h4>`)
        .replace(/\n### ([^\n]+)/g, (_, text) =>
            `\n<h3>${text}</h3>`)
        .replace(/\n## ([^\n]+)/g, (_, text) =>
            `\n<h2>${text}</h2>`)
        .replace(/\n# ([^\n]+)/g, (_, text) =>
            `\n<h1>${text}</h1>`)
        .replace(/\n---\n\n/g,
            `\n<hr />\n\n`)
        .replace(/`([^`\n]+)`/g, (_, code) =>
            `<code>${code}</code>`)
        .replace(/\b__([^_\n]+)__\b/g, (_, strong) =>
            `<strong>${strong}</strong>`)
        .replace(/\*\*([^*\n]+)\*\*/g, (_, strong) =>
            `<strong>${strong}</strong>`)
        .replace(/~~([^~\n]+)~~/g, (_, del) =>
            `<del>${del}</del>`)
        .replace(/\b_([^_\n]+)_\b/g, (_, emphasis) =>
            `<em>${emphasis}</em>`)
        .replace(/\*([^*\n]+)\*/g, (_, emphasis) =>
            `<em>${emphasis}</em>`)
        .replace(/<(https?:\/\/[^>]+)>/g, (_, url) =>
            `§a href="${url}"${target(url)}¶${url}§/a¶`)
        .replace(/\n```([a-z]*)\n([ -~¡-🫶\n]+)\n```\n\n/g, (_, syntax, pre) =>
            `\n<pre data-syntax="${syntax || 'plain'}">${pre.replace(/\n\n/g, '\n∞\n')}\n</pre>\n\n`)
        .replace(/\n(> [ -~¡-🫶\n]+?)\n\n/g, (_, blockquote) => // '?' for a non-greedy capture
            `\n<blockquote>${blockquoteMdToHtml(blockquote)}\n</blockquote>\n\n`)
        .replace(/\n(- [ -~¡-🫶\n]+?)\n\n/g, (_, ul) =>
            `\n<ul>${ulMdToHtml(ul)}\n</ul>\n\n`)
        .replace(/\n(1\. [ -~¡-🫶\n]+?)\n\n/g, (_, ol) =>
            `\n<ol>${olMdToHtml(ol)}\n</ol>\n\n`)
        .replace(/!\[([ -~¡-🫶]+)\]\(\s*([-?#:_./0-9a-z]+)\)/gi, (_, alt, url) =>
            `<img src="${url}" alt="${alt}" />`)
        .replace(/!\[([ -~¡-🫶]+)\]\(\s*([-?#:_./0-9a-z]+)\s+"([ -~¡-🫶]+)"\)/gi, (_, alt, url, title) =>
            `<img src="${url}" alt="${alt}" title="${title}" />`)
        .replace(/\[([ -~¡-🫶]+)\]\(\s*([-?#:_./0-9a-z]+)\)/gi, (_, text, url) =>
            `<a href="${url}"${target(url)}>${text}</a>`)
        .replace(/\[([ -~¡-🫶]+)\]\(\s*([-?#:_./0-9a-z]+)\s+"([ -~¡-🫶]+)"\)/gi, (_, text, url, title) =>
            `<a href="${url}"${target(url)} title="${title}">${text}</a>`)
        .replace(/§/g, '<')
        .replace(/¶/g, '>')
        .split('\n')
        .reduce((acc, line, i, lines) => {
            const [prevOut, isInParagraph] = acc;
            if (isInParagraph) {
                if (line === '') {
                    prevOut[i-1] += '</p>';
                    return [[...prevOut, ''], false]; // finish paragraph
                }
                return [[...prevOut, line], true]; // continue the paragraph
            }
            if (line !== '' && (i === 0 || lines[i-1] === '')) {
                if (line[0] !== ' ' && line[0] !== '\t' && line[0] !== '<')
                    return [[...prevOut, '<p>' + line], true]; // begin a paragraph
                if (line[0] === '<' && /^<(a |code|del|em|img |strong)/.test(line))
                    return [[...prevOut, '<p>' + line], true]; // begin a paragraph
            }
            return [[ ...prevOut, line], false]; // continue not being in a paragraph
        }, [[], false])[0]
        .join('\n')
        .replace(/∞/g, '') // prevented <p> being added in a <pre> with blank lines
        .slice(1, -2) // remove the newlines added at the start
;


/* --------------------------------- PRIVATE -------------------------------- */

const blockquoteMdToHtml = blockquoteMarkdown =>
    '\n  ' +
    blockquoteMarkdown
        .split('\n')
        .map(line => line.replace(/^>\s+/, '')) // remove leading ">" and space(s)
        .join('\n  ')
    ;

const olMdToHtml = orderedListMarkdown =>
    '\n  <li>' +
    orderedListMarkdown
        .split('\n')
        .map(li => li.replace(/^\d+\.\s+/, '')) // remove "1. " or "97. "
        .join('</li>\n  <li>') +
    '</li>';

const ulMdToHtml = unorderedListMarkdown =>
    '\n  <li>' +
    unorderedListMarkdown
        .split('\n')
        .map(li => li.replace(/^-\s+/, '')) // remove leading dash and space(s)
        .join('</li>\n  <li>') +
    '</li>';

const target = url =>
    url.slice(0, 4) === 'http' ? ' target="_blank"' : '';
