// scripts/build-pages.js

import { cpSync, existsSync, mkdirSync, readdirSync, readFileSync, renameSync, rmSync, writeFileSync } from 'node:fs';
import { dirname, resolve } from 'node:path';

import { buildPage } from './build-pages/build-page.js';
import { cacheContent } from './build-pages/cache-content.js';
import { cacheTemplates } from './build-pages/cache-templates.js';
import { green, red } from './utilities/ansi.js';

export const buildPages = (pwd) => {
    // Delete the temporary output folder, if it still exists after a previous
    // failed run of buildPages().
    const tmpDir = resolve(pwd, 'public-tmp');
    if (existsSync(tmpDir)) rmSync(tmpDir, { recursive:true, force:true });

    // Read all of the content from .md files into memory. This also checks that
    // the content exists, and is valid.
    const absPathContent = resolve(pwd, 'src/pages/content');
    const contentItems = cacheContent(absPathContent, { readdirSync, readFileSync, resolve });

    // Read all of the templates from .html files into memory. This also checks
    // that the templates exist, and are valid.
    const absPathTemplates = resolve(pwd, 'src/pages/templates');
    const templates = cacheTemplates(absPathTemplates, { dirname, readdirSync, readFileSync, resolve });
    
    // Create a temporary folder for the generated files and folders.
    mkdirSync(tmpDir);

    // Generate each page, and write it to the temporary folder.
    Object.values(contentItems).forEach(contentItem => buildPage(
        contentItem,
        { dirname, mkdirSync, resolve, writeFileSync },
        templates,
        tmpDir,
    ));

    // Copy images to 'img/' in the temporary folder.
    cpSync(
        resolve(absPathTemplates, 'img'),
        resolve(tmpDir, 'img'),
        { recursive: true },
    );

    // Copy .jpg images to 'img/' in the temporary folder.
    // const imgDir = resolve(tmpDir, 'img');
    // mkdirSync(imgDir);
    // const absPathTemplatesImg = resolve(absPathTemplates, 'img');
    // readdirSync(absPathTemplatesImg)
    //     .filter(item => item.slice(-4) === '.jpg')
    //     .forEach(item => cpSync(
    //         resolve(absPathTemplatesImg, item),
    //         resolve(imgDir, item),
    //     ));

    // Delete the public output folder, if it still exists after a previous run
    // of buildPages(). Then rename the temporary folder to be the new output
    // folder.
    const outputDir = resolve(pwd, 'public');
    if (existsSync(outputDir)) rmSync(outputDir, { recursive:true, force:true });
    renameSync(tmpDir, outputDir);
};

// Run the build.
try {
    buildPages('./');
} catch (err) {
    const place = err.stack?.split('\n') // split into lines
        .find(line => /^\s+at /.test(line)) // the first line starting "    at "
        .split('/').pop() // text after the last "/", eg "some-file.js:344:5)"
        || 'unknown location'; // fallback
    console.error(`${red('ERROR')}${err.message}\n(${place})\n`);
    const tmpDir = resolve('./', 'public-tmp');
    if (existsSync(tmpDir)) rmSync(tmpDir, { recursive:true, force:true });
    process.exit(1);
}

console.info(`${green('OK')}All pages were successfully built`);
