---
created: 20240706
route: itinerary-sunday-14th-july.html
title: Itinerary Sunday 14th
---

# itinerary Sunday 14th

---

## Morning and afternoon

__BEACH DAY 2__

Sunday daytime is for whatever people want to do, individually, again. Beach/pool/nature walks, or visit central Lisbon.

## Evening

__Maybe dinner at the Pool Party Villa again, or maybe we can have a beach BBQ…?__

- Start time TBC…

## Nighttime

__A toast to Lizzy!__

<br>

---

## [◂ Saturday 13th](./itinerary-saturday-13th-july.html)

## [▴ Back to homepage](./index.html)
