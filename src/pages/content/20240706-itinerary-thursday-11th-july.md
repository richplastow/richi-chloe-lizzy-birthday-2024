---
created: 20240706
route: itinerary-thursday-11th-july.html
title: Itinerary Thursday 11th July
---

# itinerary Thursday 11th July

---

## Morning

__Arrivals and check in to hotels.__

Most people are in the Graça area, and should be checked in by about 4pm.

## Afternoon

__Richi’s very special walking tour of central Lisbon!__

This has been months in the planning! __*You’ll need some grippy shoes,*__
because there are steep cobbled (very pretty) lanes to walk up and down. Bring
something long-sleeved, it can sometimes get a little chilly in the evenings.

- Meet at [Miradouro da Graça](https://maps.app.goo.gl/YXsRxXyedXfHGX7D9) at 6pm
- Walk to Miradouro de São Pedro de Alcântara, arriving about 7pm
- Walk to 8a Graça taproom, arriving about 8pm

[The 8a Graça taproom](https://maps.app.goo.gl/2EHB46ogHsef4chR8) serves very
good beers, which you can take to the nearby sunset viewpoint.

## Evening and Nighttime

__Dinner at Maria Food Hub.__

We booked 20 people for 9pm til 12:30am,  
[R. Maria Andrade 38, 1170-216 Lisboa, Portugal](https://maps.app.goo.gl/F7ziZxDEq82GoqUw5)

It’s healthy, tasty, and has good vegan options. It’s also pretty cheap, maybe
€20 per person for 2 courses, plus €22 per bottle of wine. And they are super
nice and friendly!

I think this menu is right:  
<https://www.thefork.co.uk/restaurant/maria-food-hub-r717526/menu>

<br>

---

## [▴ Back to homepage](./index.html)

## [Friday 12th ▶](./itinerary-friday-12th-july.html)
