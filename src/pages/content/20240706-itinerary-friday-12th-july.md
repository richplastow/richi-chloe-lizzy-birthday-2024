---
created: 20240706
route: itinerary-friday-12th-july.html
title: Itinerary Friday 12th
---

# itinerary Friday 12th

---

## Morning

__SHARED BREAKFAST PICNIC IS CANCELLED!__

We will meet at Beth and Richi’s apartment at 12:30.

## Afternoon

__Buffet lunch at Beth and Richi’s apartment.__

- At 12:30pm, we assemble at Beth and Richi’s apartment for lunch:

R + B’s ADDRESS: [Rua Infanta Dona Beatriz 7, R/C Dto, 2800-264 Almada](https://maps.app.goo.gl/3T7d7kAy8aMvxRHW9)

__Richi’s walking tour of secret Almada__

- Leaving our luggage at B + R’s apartment, we head off at 2:30pm
- Walk to Jardim do Castelo viewpoint, arriving about 2:45pm
- Walk to [Casa da Cerca,](https://maps.app.goo.gl/fKBfnKT915ysBUSS8) arriving about 3pm, and hang out in the botanical gardens<br>(there’s a really cute café with home made cake)
- Take the ‘Panoramic Elevator’ down to Jardim do Rio at 3:30pm
- Walk along Rua do Ginjal, check out the street art
- Arrive back at Beth and Richi’s apartment at about 4pm

## Evening

__Check in to accommodation near the Pool Party Villa.__

We will organise some Ubers to take people and their bags from B + R’s apartment
to their AirBnBs and hotels in the Aroeira / Fonte da Telha area. I’d guess
about €5 per person.

EXPECT TO ARRIVE AT YOUR ACCOMMODATION AT ABOUT 5PM.

BBQ dinner at the Pool Party Villa — Richi's birthday party!

- We will get the tunes on and light the BBQ around 5:30pm
- Aiming to eat at maybe 7 or 8pm
- Tropical theme, bring your brightest outfit!

VILLA ADDRESS: [(the grey roof) Rua de São Roque 32, Aroeira, Setúbal 2820-126](https://maps.app.goo.gl/W5pcjfuCer3HkP5v7)

## Nighttime

__PARTY times 🎵✨__

No noise is allowed outside the villa after 11pm, so we’ll take it indoors at that point.

<br>

---

## [◂ Thursday 11th](./itinerary-thursday-11th-july.html)

## [Saturday 13th ▶](./itinerary-saturday-13th-july.html)