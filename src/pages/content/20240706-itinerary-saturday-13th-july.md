---
created: 20240706
route: itinerary-saturday-13th-july.html
title: Itinerary Saturday 13th
---

# itinerary Saturday 13th

---

## Morning and afternoon

__BEACH DAY 1__

Saturday daytime is for whatever people want to do, individually. There's a lovely beach, and an amazing hike/walk in a nature reserve, both nearby. You could visit central Lisbon (about 45 minutes away, about €5 per-person each way) for museums, art galleries and boutique shopping. Or just hang out at the Pool Party Villa, to swim and sunbathe

Here is the beach location — to meet there, we will send a pin 📍 for our spot <https://maps.app.goo.gl/CJqF4nTt4jzBeKF9A>

## Evening

__Dinner at the Pool Party Villa__<br>
__Chloe's (slightly belated) birthday party!__

- Start time TBC…
- Theme TBC…

## Nighttime

__Pool party till 11pm,__ party inside after

<br>

---

## [◂ Friday 12th](./itinerary-friday-12th-july.html)

## [Sunday 14th ▶](./itinerary-sunday-14th-july.html)
