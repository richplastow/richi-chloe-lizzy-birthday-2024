---
created: 20240305
route: plan-for-the-long-weekend-v1.html
title: Plan for the long weekend (old plans)
---

# Plan for the long weekend (old plans)

__This plan is feeling pretty solid now. Some details will be added, but the basic structure won't change.__

## Thursday 11th July
1. People flying to Lisbon throughout the day on Thurs 11th - see the [Choosing flights from/to London](choosing-flights-from-to-london.html) page
2. People check in to their hotels in the Graça area - see the [Choosing Accommodation](choosing-accommodation.html) page
3. Thursday night out in the Graça and Anjos areas of Lisbon - maybe dinner at [Maria Food Hub](https://www.mariafoodhub.com/)

## Friday 12th July
1. Friday brunch, stroll around town, see the sights, nice long lunch
2. Friday afternoon, head to to Fonte de Telha beach. _Possibly_ this could be sailing in a chartered yacht from central Lisbon waterfront, but that idea may be unrealistic!
3. Beach time
4. People check in to their apartments and guest houses near Fonte da Telha - see the [Choosing Accommodation](choosing-accommodation.html) page
5. Meet for dinner at the poolparty villa - probably [this AirBnB at Rua Carlos Carneiro 2,](https://www.airbnb.com/rooms/855649489322263401) but if not, a similar place nearby
6. Friday evening pool party at the villa - Richi's birthday party

## Saturday 13th July
1. Saturday daytime is for whatever people want to do, individually. There's a lovely beach, and an amazing hike/walk in a nature reserve, both nearby. You could visit central Lisbon (about 45 minutes away, £5 per-person each way) for museums, art galleries and boutique shopping. Or just hang out at the poolparty villa, to swim and sunbathe
2. Meet for dinner at the poolparty villa
3. Saturday evening pool party at the villa - Chloe's (slightly belated) birthday party

## Sunday 14th July
1. Sunday daytime is for whatever people want to do, individually, again. Beach/pool/nature walks, or visit central Lisbon
2. Meet for dinner at the poolparty villa
3. Sunday evening pool party at the villa - Lizzy's birthday party

## Monday 15th July
- Many people will be checking out on Monday morning and heading homeward, but of course you can extend your trip and stay longer

---

## [Back to homepage](./index.html)
