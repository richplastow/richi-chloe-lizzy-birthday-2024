---
created: 20240302
route: choosing-flights-from-to-london.html
title: Choosing flights from/to London
---

# Choosing flights from/to London

Here's some flights from London Airports (Gatwick, Heathrow, Luton and Stansted)
to Lisbon Airport, and back again. I searched for single tickets, up to £100
each way, but of course there are more expensive options out there.

All prices are for direct tickets, one-way, one adult, booking directly from the
airlines' own websites, without any extras like baggage.

## London -> Lisbon

### Tuesday 9th July

- 07:00 £55 easyJet Luton
- 08:00 £100 TAP Heathrow
- 09:35 £68 Ryanair Stansted
- 10:05 £81 easyJet Gatwick
- 12:25 £67 easyJet Luton
- 15:35 £57 easyJet Luton
- 16:25 £56 Ryanair Stansted
- 17:15 £65 easyJet Gatwick
- 18:20 £56 Wizz Luton
- 20:45 £56 Ryanair Stansted

### Wednesday 10th July

- 05:55 £77 Ryanair Stansted
- 13:15 £89 Ryanair Stansted
- 15:50 £98 easyJet Gatwick
- 17:10 £78 Ryanair Stansted
- 18:10 £85 easyJet Luton
- 18:20 £72 Wizz Luton

### Thursday 11th July

- 06:40 £87 easyJet Luton
- 12:25 £96 easyJet Luton
- 17:40 £77 easyJet Luton (lands 20:25, too late for our big evening dinner)
- 18:20 £90 Wizz Luton (lands 21:15, too late for any evening activities)
- 19:20 £90 Ryanair Stansted (lands 22:10, too late for any evening activities)

## Lisbon -> London

Ryanair and Wizz prices below are converted from Euros to Pounds, where
1.00 EUR = 0.86 GBP.

### Monday 15th July

- 06:40 £54 Ryanair Stansted
- 06:40 £75 easyJet Gatwick
- 07:05 £98 TAP Gatwick
- 21:55 £85 Wizz Luton

### Tuesday 16th July

- 06:40 £45 easyJet Gatwick
- 07:05 £98 TAP Gatwick
- 07:40 £98 TAP Heathrow
- 10:25 £62 easyJet Luton
- 12:50 £48 Ryanair Stansted
- 15:05 £98 TAP Heathrow
- 16:10 £98 TAP Heathrow
- 16:55 £90 easyJet Gatwick
- 17:35 £46 Ryanair Stansted
- 19:20 £98 TAP Heathrow
- 20:10 £53 Ryanair Stansted
- 20:15 £98 TAP Heathrow
- 20:45 £43 easyJet Gatwick
- 21:55 £57 Wizz Luton

### Wednesday 17th July

- 06:40 £35 easyJet Gatwick
- 07:05 £98 TAP Gatwick
- 07:40 £98 TAP Heathrow
- 09:10 £30 Ryanair Stansted
- 10:05 £98 TAP Heathrow
- 10:25 £58 easyJet Luton
- 12:30 £66 easyJet Gatwick
- 12:55 £98 TAP Gatwick
- 13:10 £41 Ryanair Stansted
- 15:05 £98 TAP Heathrow
- 15:45 £73 easyJet Luton
- 16:10 £98 TAP Heathrow
- 16:30 £40 Ryanair Stansted
- 17:20 £60 easyJet Gatwick
- 19:25 £98 TAP Heathrow
- 20:15 £98 TAP Heathrow
- 20:25 £29 Ryanair Stansted
- 21:25 £28 easyJet Luton
- 21:55 £34 Wizz Luton

## Example trips:

__Long trip, midweek flights:__

8 nights, spending £83 on flights (plus extra for baggage).

- Tuesday 9th July 18:20 £56 Wizz Luton -> Lisbon
- Wednesday 17th July 20:25 £29 Ryanair Lisbon -> Stansted

<!-- If you spend £309 on accommodation, and spend £40 per day on food/drink/travel,
that's a total of £750. -->

__Short trip, early morning flights:__

4 nights, spending £141 on flights (plus extra for baggage).

- Thursday 11th July 06:40 £87 easyJet Luton -> Lisbon
- Monday 15th July 06:40 £54 Ryanair Lisbon -> Stansted

<!-- If you spend £161 on accommodation, and spend £45 per day on food/drink/travel,
that's a total of £525. -->

__Medium length trip, using Gatwick in the middle of the day:__

6 nights, spending £188 on flights (plus extra for baggage).

- Wednesday 10th July 15:50 £98 easyJet Gatwick -> Lisbon
- Tuesday 16th July 16:55 £90 easyJet Lisbon -> Gatwick

<!-- If you spend £270 on accommodation, and spend £50 per day on food/drink/travel,
that's a total of £800. -->

---

## [Back to homepage](./index.html)

Researched by Richi, on Saturday March 2nd.
