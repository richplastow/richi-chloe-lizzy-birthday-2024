---
created: 20240302
route: choosing-accommodation.html
title: Choosing accommodation
---

# Choosing accommodation

Here's some Lisbon hotel and AirBnB options. I looked for places between £30 and
£65 per person per night, but of course there are cheaper and more expensive
options out there.

## Hotels in the Anjos / Graça areas of Lisbon

All prices are per-person based on two people sharing, for a double room with en
suite bathroom. Extras like breakfast or airport-transfer are not included.

I only searched on booking.com, and I used these filters:  
'Private Bathroom', 'Shower', 'Very good 8+', 'Air conditioning', 'Hotels'

All three of these hotels are currently available Tuesday 9th July to Friday
12th July. They all charge the same per night whether you book 1, 2 or 3 nights.

### Tings Lisbon

- Fabulous 8.9
- £53 per-person per-night
- 11 minutes walk to Maria Food Hub
- 9 minutes walk to Miradouro da Graça
- 5 minutes walk to 8a Graça taproom
- [https://www.booking.com/Share-6YgZFt](https://www.booking.com/Share-6YgZFt)

### Lisbon 5 Hotel

- Very good 8.1
- £56 per-person per-night
- 7 minutes walk to Maria Food Hub
- 13 minutes walk to Miradouro da Graça
- 8 minutes walk to 8a Graça taproom
- [https://www.booking.com/Share-M5dbid](https://www.booking.com/Share-M5dbid)

### Loios Studios and Apartments

- Very good 8.4
- £65 per-person per-night
- 20 minutes walk to Maria Food Hub
- 5 minutes walk to Miradouro da Graça
- 10 minutes walk to 8a Graça taproom
- [https://www.booking.com/Share-lY9Nd0](https://www.booking.com/Share-lY9Nd0)

## Guesthouse rooms near to the poolparty villa, and near Fonte da Telha beach

Both of these guesthouse rooms are currently available Friday 12th July to
Wednesday 17th July. Prices shown here are per-person per-night, based on two
people sharing. Distances may be wrong, AirBnB hides the exact location until
you book.

### Zion guest house - Casal

- Guest favorite 5.0 (5 reviews)
- £35 per-person per-night
- 11 minutes walk to poolparty villa (Rua Carlos Carneiro 2)
- 9 minutes cycle to Fonte da Telha beach
- [https://www.airbnb.com/rooms/976746376764262079](https://www.airbnb.com/rooms/976746376764262079)

### AROEIRA - Close to Lisbon and beaches

- Guest favorite 4.94 (35 reviews)
- £37 per-person per-night
- 25 minutes walk to poolparty villa (Rua Carlos Carneiro 2)
- 12 minutes cycle to Fonte da Telha beach
- [https://www.airbnb.com/rooms/15269841](https://www.airbnb.com/rooms/15269841)

## 1-bedroom apartments near to the poolparty villa, and near Fonte da Telha beach

Both of these apartments are currently available Friday 12th July to Wednesday
17th July. Prices shown here are per-person per-night, based on two people
sharing. Distances may be wrong, AirBnB hides the exact location until you book.

### Aroeira Sea GuestHouse

- 4.78 (27 reviews)
- £35 per-person per-night
- 25 minutes walk to poolparty villa (Rua Carlos Carneiro 2)
- 12 minutes cycle to Fonte da Telha beach
- [https://www.airbnb.com/rooms/50827916](https://www.airbnb.com/rooms/50827916)

### Studio close to beach

- 4.55 (11 reviews)
- £33 per-person per-night
- 32 minutes walk to poolparty villa (Rua Carlos Carneiro 2)
- 12 minutes cycle to Fonte da Telha beach
- [https://www.airbnb.com/rooms/953618889220142372](https://www.airbnb.com/rooms/953618889220142372)

## 2-bedroom apartments near to the poolparty villa, and near Fonte da Telha beach

Both of these apartments are currently available Friday 12th July to Wednesday
17th July. Prices shown here are per-person per-night, based on four people
sharing. Distances may be wrong, AirBnB hides the exact location until you book.

### Aroeira Bliss

- Guest favorite 4.86 (79 reviews)
- £30 per-person per-night
- 20 minutes walk to poolparty villa (Rua Carlos Carneiro 2)
- 12 minutes cycle to Fonte da Telha beach
- [https://www.airbnb.com/rooms/50059628](https://www.airbnb.com/rooms/50059628)

### Beach, surf and golf apartment

- 4.32 (19 reviews)
- £33 per-person per-night
- 10 minutes walk to poolparty villa (Rua Carlos Carneiro 2)
- 9 minutes cycle to Fonte da Telha beach
- [https://www.airbnb.com/rooms/41238592](https://www.airbnb.com/rooms/41238592)

## Example trips:

__Long trip, cheaper accommodation:__

8 nights, spending £309 on accommodation. The Aroeira Bliss AirBnB assumes four
people sharing two bedrooms.

- Tuesday 9th to Friday 12th July, 3 nights at Tings = £159 per-person
- Friday 12th to Wednesday 17th July, 5 nights at Aroeira Bliss = £150 per-person

<!-- If you spend £83 on flights, and spend £40 per day on food/drink/travel, that's
a total of £750. -->

__Short trip, quite nice accommodation:__

4 nights, spending £161 on accommodation.

- Thursday 11th to Friday 12th July, 1 night at Lisbon 5 Hotel = £56 per-person
- Friday 12th to Monday 15th July, 3 nights at Zion guest house = £105 per-person

<!-- If you spend £141 on flights, and spend £45 per day on food/drink/travel, that's
a total of £525. -->

__Medium length trip, nicest accommodation:__

6 nights, spending £270 on accommodation.

- Wednesday 10th to Friday 12th July, 2 nights at Loios Studios = £130 per-person
- Friday 12th to Tuesday 16th July, 4 nights at Aroeira Sea GuestHouse = £140 per-person

<!-- If you spend £188 on flights, and spend £50 per day on food/drink/travel, that's
a total of £800. -->

---

## [Back to homepage](./index.html)

Researched by Richi, on Saturday March 2nd.
