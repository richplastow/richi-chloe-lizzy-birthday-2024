const { Plugin } = await import('./plugin.js');

export class UserCore extends Plugin {

    // Define default core user and user-interface settings.
    #ui = {
        theme: 'LIGHT' // DARK|LIGHT
    };
    #user = {
        theme: 'SYSTEM' // DARK|LIGHT|SYSTEM
    };

    init(app) {
        const pcsD = '(prefers-color-scheme: dark)';

        // Hook up a mutator for the user's 'theme' setting, and the ui's 'theme' mode.
        app.on('set.user.theme', value => {
            value = value || this.#user.theme; // first ever load, or OS theme change
            if (! /^(DARK|LIGHT|SYSTEM)$/.test(value)) throw Error('UserCore::init() 38716');
            this.#user.theme = value;
            if (window.localStorage) localStorage.setItem('RCLB24v1.user.theme', value);
            this.#ui.theme = value !== 'SYSTEM' ? value :
                window.matchMedia && matchMedia(pcsD).matches ? 'DARK' : 'LIGHT';
            document.body.classList[this.#ui.theme==='DARK'?'remove':'add']('light');
        });

        // As the page loads, initialize the user's theme choice based on the value in
        // localStorage (if it exists). Otherwise default to 'SYSTEM'.
        app.emit('set.user.theme',
            window.localStorage && localStorage.getItem('RCLB24v1.user.theme'));

        // When the OS theme (dark-mode) changes, trigger the 'set.user.theme' handler,
        // which will query the OS theme if the user has chosen 'SYSTEM'.
        if (window.matchMedia) matchMedia(pcsD).addEventListener('change',
            function() { app.emit('set.user.theme') } );

        super.init(app); // set `didInit` to `true` and keep a ref to `app`
    }
}
